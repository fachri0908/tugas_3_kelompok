<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller {
    public function index() {
        if(Auth::check()) { // Auth::check akan memeriksa apakah user sudah login atau belum
            if(Auth::user()->roleId == 1) {
                // jika user dengan roleId 1 (ADMIN), akan ditampilkan halaman dashboard admin
                return view('pages.admin.dashboard', ['user'=> Auth::user()]);
            } else if(Auth::user()->roleId == 2){
                // jika user dengan roleId 2 (STAFF), akan ditampilkan halaman dashboard staff
                return view('pages.staff.dashboard', ['user'=> Auth::user()]);
            } else if(Auth::user()->roleId == 3){
                 // jika user dengan roleId 3 (MANAGER), akan ditampilkan halaman dashboard manager
                 return view('pages.manager.dashboard', ['user'=> Auth::user()]);
            }
        }

        return redirect("login"); // user yang belum login akan dikembalikan ke halaman login
    }
}
