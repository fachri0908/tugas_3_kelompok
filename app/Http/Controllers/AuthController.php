<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {
    public function index() {
        return view('pages.login');
    }  
      
    public function authenticate(Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]); //function validate akan memvalidasi input email dan password untuk memastikan email dan password sudah diinput
   
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect('/');
        } 
        //function attempt akan mencocokkan input user dengan data yang ada di database, 
        // jika username dan password benar, maka user akan berhasil login, dan dialihkan ke halaman dashboard
  
        return redirect("login")->withErrors('Login details are not valid');
        // jika input salah, maka user akan dikembalikan ke halaman login, dengan return pesan error
    }
    
    public function signOut() {
        Session::flush(); // membersihkan semua session user aktif
        Auth::logout(); // logout user dari aplikasi
  
        return Redirect('login');
    }
}