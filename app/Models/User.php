<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'email',
        'password',
        'name',
        'employeeId',
        'roleId',
        'address',
        'phoneNumber',
        'status'
    ];
    // $fillable mengindikasikan field field yang dapat diisi oleh user untuk penginputan data

    protected $hidden = [
        'password',
    ];
    // $hidden mengindikasikan field yang akan dihide atau tidak di return saat user igin melihat data / read

    public function role() {
        return $this->hasOne(Role::class, 'id', 'roleId');
    }
    // function role digunakan untuk mendapatkan data role user sesuai dengan roleID
}
