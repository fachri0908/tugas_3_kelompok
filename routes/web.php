<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;

Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('authenticate', [AuthController::class, 'authenticate'])->name('authenticate'); 
Route::get('/signout', [AuthController::class, 'signOut'])->name('signout');
Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
