@extends('../layout/master')

@section('sidebar')
    @include('../layout/admin/sidebar')
@stop

@section('content')
    <!-- Sale & Revenue Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Total Salary</h5>
                            <br/>
                            <h1>Rp 4 M</h1>
                            <h5 class="mb-1" style="color: green;">%4</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Average Salary</h5>
                            <br/>
                            <h1>Rp 5 Jt</h1>
                            <h5 class="mb-1" style="color: red;">-%2</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Turnover Rate</h5>
                            <br/>
                            <h1>13%</h1>
                            <h5 class="mb-1" style="color: green;">%3.1</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Absenteeism Rate</h5>
                            <br/>
                            <h1>5%</h1>
                            <h5 class="mb-1" style="color: red;">%3</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sale & Revenue End -->

    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Average Age</h5>
                            <br/>
                            <h1>69</h1>
                            <h5 class="mb-1" style="color: green;">%4</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Permanent Rate</h5>
                            <br/>
                            <h1>80%</h1>
                            <h5 class="mb-1" style="color: green;">%2.1</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Hired</h5>
                            <br/>
                            <h1>5</h1>
                            <h5 class="mb-1" style="color: green;">%1</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
            <div class="col-sm-3 col-xl-3">
                <div class="bg-secondary rounded h-100 p-4">
                        <div class="testimonial-item text-center">
                            <h5 class="mb-1">Left</h5>
                            <br/>
                            <h1>12</h1>
                            <h5 class="mb-1" style="color: green;">%5</h5>
                            <p class="mb-2">vs previous month</p>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Widgets Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-6 col-md-6 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Total Employees per Month</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="fte-chart"></canvas>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Total Salary</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="fte-chart2"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- Widgets End -->


    <!-- Sales Chart Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Employee Age Demography</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="demo-chart"></canvas>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Employee Gender Demography</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="gender-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- Sales Chart End -->
@stop
@section('js')
    <script>
        // Pie Chart
        var ctx5 = $("#demo-chart").get(0).getContext("2d");
        var myChart5 = new Chart(ctx5, {
            type: "pie",
            data: {
                labels: ["19-25yo", "25-30yo", "30-35yo", "35-40yo", ">40yo"],
                datasets: [{
                    backgroundColor: [
                        "rgba(235, 22, 22, .7)",
                        "rgba(235, 22, 22, .6)",
                        "rgba(235, 22, 22, .5)",
                        "rgba(235, 22, 22, .4)",
                        "rgba(235, 22, 22, .3)"
                    ],
                    data: [55, 49, 44, 24, 15]
                }]
            },
            options: {
                responsive: true
            }
        });
        // Pie Chart
        var ctx5 = $("#gender-chart").get(0).getContext("2d");
        var myChart5 = new Chart(ctx5, {
            type: "pie",
            data: {
                labels: ["Male", "Female"],
                datasets: [{
                    backgroundColor: [
                        "rgba(235, 22, 22, .7)",
                        "rgba(235, 22, 22, .3)"
                    ],
                    data: [112,75]
                }]
            },
            options: {
                responsive: true
            }
        });

        // Single Line Chart
        var ctx3 = $("#fte-chart").get(0).getContext("2d");
        var myChart3 = new Chart(ctx3, {
            type: "line",
            data: {
                labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember" ],
                datasets: [{
                    label: "Karyawan",
                    fill: false,
                    backgroundColor: "rgba(235, 22, 22, .7)",
                    data: [75, 92, 80, 70, 60, 70, 75, 92, 80, 70, 60, 70, ]
                }]
            },
            options: {
                responsive: true
            }
        });

        var ctx3 = $("#fte-chart2").get(0).getContext("2d");
        var myChart3 = new Chart(ctx3, {
            type: "line",
            data: {
                labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "July", "Agustus", "September", "Oktober", "November", "Desember" ],
                datasets: [{
                    label: "Gaji (Miliyar)",
                    fill: false,
                    backgroundColor: "rgba(235, 22, 22, .7)",
                    data: [1.2, 1.3, 1.4, 1.5,1.8, 1.8,2.2, 2.8,3, 3.4,3.2, 4]
                }]
            },
            options: {
                responsive: true
            }
        });
        
    </script>
@stop
@section('styles')
    <style>
        .icon-button__badge {
        position: absolute;
        top: -10px;
        right: -10px;
        width: 25px;
        height: 25px;
        background: red;
        color: #ffffff;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
    }
    </style>
@stop