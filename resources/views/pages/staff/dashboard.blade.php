@extends('../layout/master')

@section('sidebar')
    @include('../layout/staff/sidebar')
@stop

@section('content')
    <!-- Benefits and leave -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-6 col-xl-4">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-line fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Plafon Pengobatan</p>
                        <h6 class="mb-0">Sisa : Rp. 4.000.000</h6>
                        <h6 class="mb-0"> Dari total : Rp. 4.500.000</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-bar fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Cuti</p>
                        <h6 class="mb-0">Sisa : 8/12</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-area fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Surat Perintah Lembur</p>
                        <h6 class="mb-0">Total : 3</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-chart-pie fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Absen Tidak Lengkap</p>
                        <h6 class="mb-0">1</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-keyboard fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Terlambat</p>
                        <h6 class="mb-0">0</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-4">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-clock fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Years of Work</p>
                        <h6 class="mb-0">3 Tahun 3 Bulan</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Benefits and leave End -->

    <!-- Calendar and performance -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-12 col-md-6 col-xl-4">
                <div class="h-100 bg-secondary rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Working Calander</h6>
                        <a href="">Show All</a>
                    </div>
                    <div id="calender"></div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-xl-8">
                <div class="bg-secondary rounded h-100 p-4">
                    <h6 class="mb-4">Total Pembuatan Improvement</h6>
                    <canvas id="improvement-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- Calendar and performance end -->

    <!-- Absence -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Data Absensi</h6>
                <a href="">Show All</a>
            </div>
            <div class="table-responsive">
                <table class="table text-start align-middle table-bordered table-hover mb-0">
                    <thead>
                        <tr class="text-white">
                            <th scope="col"><input class="form-check-input" type="checkbox"></th>
                            <th scope="col">Date</th>
                            <th scope="col">Absen In</th>
                            <th scope="col">Absen Out</th>
                            <th scope="col">Lembur In</th>
                            <th scope="col">Lembur our</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input class="form-check-input" type="checkbox"></td>
                            <td>01 Jan 2023</td>
                            <td>07.00</td>
                            <td>17.00</td>
                            <td>0</td>
                            <td>0</td>
                            <td><a class="btn btn-sm btn-primary" href="">Detail</a></td>
                        </tr>
                        <tr>
                            <td><input class="form-check-input" type="checkbox"></td>
                            <td>02 Jan 2045</td>
                            <td>07.00</td>
                            <td>17.05</td>
                            <td>0</td>
                             <td>0</td>
                            <td><a class="btn btn-sm btn-primary" href="">Detail</a></td>
                        </tr>
                        <tr>
                            <td><input class="form-check-input" type="checkbox"></td>
                            <td>03 Jan 2023</td>
                            <td>07.00</td>
                            <td>17.15</td>
                            <td>0</td>
                            <td>0</td>
                            <td><a class="btn btn-sm btn-primary" href="">Detail</a></td>
                        </tr>
                        <tr>
                            <td><input class="form-check-input" type="checkbox"></td>
                            <td>04 Jan 2023</td>
                            <td>07.00</td>
                            <td>20.45</td>
                            <td>17.00</td>
                            <td>20.40</td>
                            <td><a class="btn btn-sm btn-primary" href="">Detail</a></td>
                        </tr>
                        <tr>
                            <td><input class="form-check-input" type="checkbox"></td>
                            <td>05 Jan 2023</td>
                            <td>07.00</td>
                            <td>17.30</td>
                            <td>0</td>
                            <td>0</td>
                            <td><a class="btn btn-sm btn-primary" href="">Detail</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Absence end -->
@stop
@section('js')
<script>
    // Single Line Chart
    var ctx3 = $("#improvement-chart").get(0).getContext("2d");
    var myChart3 = new Chart(ctx3, {
        type: "line",
        data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun","Jul", "Aug", "Sept", "Okt", "Nov", "Des"],
            datasets: [{
                label: "Improvement",
                fill: false,
                backgroundColor: "rgba(235, 22, 22, .7)",
                data: [2, 1, 3, 1, 2, 3,2, 4, 3, 1, 2, 3]
            }]
        },
        options: {
            responsive: true
        }
    });
</script>
@stop
