@extends('../layout/master')

@section('sidebar')
    @include('../layout/admin/sidebar')
@stop

@section('content')
    <!-- User activities -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-exclamation-circle fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Pending Task</p>
                        <h6 class="mb-0">12</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-check-circle fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Done Task</p>
                        <h6 class="mb-0">4</h6>
                    </div>
                </div>
            </div>  
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-plug fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">User Online</p>
                        <h6 class="mb-0">53</h6>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3">
                <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
                    <i class="fa fa-users fa-3x text-primary"></i>
                    <div class="ms-3">
                        <p class="mb-2">Total User</p>
                        <h6 class="mb-0">152</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- User activities End -->

    <!-- Works and Calendar -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-12 col-md-6 col-xl-8">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Full-time Equivalent</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="fte-chart"></canvas>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-xl-4">
                <div class="h-100 bg-secondary rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Working Calender</h6>
                        <a href="">Show All</a>
                    </div>
                    <div id="calender"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Works and Calendar -->

    <!-- Demography -->
    <div class="container-fluid pt-4 px-4">
        <div class="row g-4">
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Employee Age Demography</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="demo-chart"></canvas>
                </div>
            </div>
            <div class="col-sm-12 col-xl-6">
                <div class="bg-secondary text-center rounded p-4">
                    <div class="d-flex align-items-center justify-content-between mb-4">
                        <h6 class="mb-0">Employee Gender Demography</h6>
                        <a href="">Show All</a>
                    </div>
                    <canvas id="gender-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- Demography End -->
@stop

@section('js')
    <script>
        // Pie Chart
        var ctx5 = $("#demo-chart").get(0).getContext("2d");
        var myChart5 = new Chart(ctx5, {
            type: "pie",
            data: {
                labels: ["19-25yo", "25-30yo", "30-35yo", "35-40yo", ">40yo"],
                datasets: [{
                    backgroundColor: [
                        "rgba(235, 22, 22, .7)",
                        "rgba(235, 22, 22, .6)",
                        "rgba(235, 22, 22, .5)",
                        "rgba(235, 22, 22, .4)",
                        "rgba(235, 22, 22, .3)"
                    ],
                    data: [55, 49, 44, 24, 15]
                }]
            },
            options: {
                responsive: true
            }
        });
        // Pie Chart
        var ctx5 = $("#gender-chart").get(0).getContext("2d");
        var myChart5 = new Chart(ctx5, {
            type: "pie",
            data: {
                labels: ["Male", "Female"],
                datasets: [{
                    backgroundColor: [
                        "rgba(235, 22, 22, .7)",
                        "rgba(235, 22, 22, .3)"
                    ],
                    data: [112,75]
                }]
            },
            options: {
                responsive: true
            }
        });

        // Single Line Chart
        var ctx3 = $("#fte-chart").get(0).getContext("2d");
        var myChart3 = new Chart(ctx3, {
            type: "line",
            data: {
                labels: ["HR&GA", "IT", "Finance", "Purchasing", "Marketing", "Accounting"],
                datasets: [{
                    label: "FTE",
                    fill: false,
                    backgroundColor: "rgba(235, 22, 22, .7)",
                    data: [75, 92, 64, 57, 82, 73]
                }]
            },
            options: {
                responsive: true
            }
        });
    </script>
@stop

@section('styles')
    <style>
        .icon-button__badge {
        position: absolute;
        top: -10px;
        right: -10px;
        width: 25px;
        height: 25px;
        background: red;
        color: #ffffff;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;
    }
    </style>
@stop
