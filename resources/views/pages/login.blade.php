<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sign In to EMS | Employee Management System</title>
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body{font-family:Nunito,sans-serif;margin:0}a{background-color:transparent}html{-webkit-text-size-adjust:100%;
                font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,
                Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}
            .antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.relative{position:relative}.flex{display:flex}
            .items-center{align-items:center}.justify-center{justify-content:center}.min-h-screen{min-height:100vh}
            .bg-gray-100{--tw-bg-opacity:1;background-color:rgb(243 244 246 / var(--tw-bg-opacity))}
            .shadow{--tw-shadow:0 1px 3px 0 rgb(0 0 0 / 0.1),0 1px 2px -1px rgb(0 0 0 / 0.1);
                --tw-shadow-colored:0 1px 3px 0 var(--tw-shadow-color),0 1px 2px -1px var(--tw-shadow-color);
                box-shadow:var(--tw-ring-offset-shadow,0 0 #0000),var(--tw-ring-shadow,0 0 #0000),var(--tw-shadow)}
            .p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.mx-auto{margin-left:auto;margin-right:auto}
            .px-8{padding-left:2rem;padding-right:2rem}.input-field{width:250px;height:30px;border-radius:5px;margin-bottom:15px}
            .login-button{width:70px;height:30px;font-family:Arial;border-radius:3px;background-color:rgb(104 112 125);border:0}.text-white{color:#fff}
            .text-grey{color:#ededed}.bg-gray-800{--tw-bg-opacity:1;background-color:rgb(31 41 55 / var(--tw-bg-opacity))}
            .bg-gray-900{--tw-bg-opacity:1;background-color:rgb(17 24 39 / var(--tw-bg-opacity))}.rounded-lg{border-radius:.5rem}
            .text-danger{color: red;}.error-message{margin-bottom:10px;}
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex justify-center min-h-screen bg-gray-900 items-center">
            <div class="mx-auto px-8">
                <div class="p-6 bg-gray-800 shadow rounded-lg">
                    <div class="mt-4 flex justify-center text-white">Sign In to EMS</div>
                    <div class="p-6">
                        <form method="POST" action="{{ route('authenticate') }}">
                            @csrf
                            {{-- function csrf merupakan protection bawaan dari laravel untuk mengatasi serangan csrf (Cross-site request forgeries) --}}
                            <div>
                                <input type="text" placeholder="Email" id="email" class="input-field" name="email" required autofocus>
                            </div>
                            <div>
                                <input type="password" placeholder="Password" id="password" class="input-field" name="password" required>
                                @if ($errors->any())
                                    <br/>
                                    <div class="error-message text-danger">
                                        {{ $errors->first() }} {{-- menampilkan pesan error yang dikirimkan oleh AuthController --}}
                                    </div>
                                @endif
                            </div>
                            <div>
                                <button type="submit" class="login-button text-grey">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>